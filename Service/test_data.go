package service

import (
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redis "gitlab.com/service31/Common/Redis"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Product"
	module "gitlab.com/service31/Data/Module/Product"
)

//SetupTesting Init test
func SetupTesting() {
	common.BootstrapTesting(false)
}

//DoneTesting close all connections
func DoneTesting() {
	defer common.DB.Close()
	defer redis.RedisClient.Close()
}

//GetTestCreateCategoryData test data
func GetTestCreateCategoryData() (*module.CreateCategoryRequest, uuid.UUID) {
	entityID := uuid.NewV4()
	return &module.CreateCategoryRequest{
		EntityID: entityID.String(),
		Name:     common.GetRandomString(5),
	}, entityID
}

//GetTestUpdateCategoryData test data
func GetTestUpdateCategoryData(id uuid.UUID) *module.UpdateCategoryRequest {
	return &module.UpdateCategoryRequest{
		ID:   id.String(),
		Name: common.GetRandomNumberString(6),
	}
}

//GetNewProductData test data
func GetNewProductData(categoryID uuid.UUID) ([]*module.CreateProductRequest, uuid.UUID) {
	entityID := uuid.NewV4()
	var requests []*module.CreateProductRequest
	for i := 0; i < 5; i++ {
		requests = append(requests, &module.CreateProductRequest{
			EntityID: entityID.String(),
			Product: &module.CreateProductItem{
				Name:        common.GetRandomString(10),
				Description: common.GetRandomString(50),
				UnitPrice:   1.0,
				MAP:         2.0,
				Brand:       common.GetRandomString(5),
				ImageURL:    "https://test.zh-code.com/" + common.GetRandomString(10) + ".png",
				UPC:         common.GetRandomNumberString(14),
				SKUNumber:   common.GetRandomNumberString(11),
				CategoryID:  categoryID.String(),
				IsActive:    true,
			},
		})
	}
	return requests, entityID
}

//GetNewInventoryData test data
func GetNewInventoryData() (string, uint64) {
	return common.GetRandomString(10), 100
}

//SetupCategory set up category
func SetupCategory(c *entity.Category) {
	bytes, err := c.MarshalBinary()
	logger.CheckFatal(err)

	mapKey := redisClient.RedisMapKeys.GetCategoryMapKey(c.EntityID.String(), c.ID.String())
	key := redisClient.RedisMapKeys.GetCategoryKey(c.ID.String())
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.CategoryHashMap, mapKey, key)
	redisClient.SetRedisValue(key, bytes)
}

//SetupProduct set up product
func SetupProduct(product *entity.Product) {
	bytes, err := product.MarshalBinary()
	logger.CheckFatal(err)

	productMapKey := redisClient.RedisMapKeys.GetProductMapKey(product.EntityID.String(), product.ID.String())
	key := redisClient.RedisMapKeys.GetProductKey(product.ID.String())
	temp := make(map[string]interface{})
	temp[productMapKey] = key
	redisClient.RedisClient.HSet(redisClient.RedisMapKeys.ProductHashMap, temp).Err()
	//redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ProductHashMap, productMapKey, key)
	redisClient.SetRedisValue(key, bytes)
}
