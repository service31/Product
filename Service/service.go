package service

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-redis/redis"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Product"
	module "gitlab.com/service31/Data/Module/Product"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//CreateCategory create category
func CreateCategory(entityID uuid.UUID, in *module.CreateCategoryRequest) (*entity.Category, error) {
	parentID, _ := uuid.FromString(in.GetParentID())

	var parent *entity.Category
	if parentID != uuid.Nil {
		parent, _ = getCategoryByID(parentID)
	}

	category := &entity.Category{
		EntityID: entityID,
		Name:     in.GetName(),
		Parent:   parent,
		Child:    nil,
	}

	if db := common.DB.Set("gorm:auto_preload", true).Save(category).Scan(&category); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to create category")
	}

	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.CategoryChange, category.ID.String()))
	return category, nil
}

//UpdateCategory updatecategory
func UpdateCategory(in *module.UpdateCategoryRequest) (*entity.Category, error) {
	categoryID, err := uuid.FromString(in.GetID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad CategoryID")
	}

	category, err := getCategoryByID(categoryID)
	if err != nil {
		return nil, err
	}

	category.Name = strings.Trim(in.GetName(), " ")

	if db := common.DB.Set("gorm:auto_preload", true).Save(category).Scan(&category); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to update category")
	}

	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.CategoryChange, category.ID.String()))
	return category, nil
}

//CreateProduct create product
func CreateProduct(entityID uuid.UUID, in *module.CreateProductRequest) (*entity.Product, error) {
	p := in.GetProduct()
	categoryID, err := uuid.FromString(p.GetCategoryID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad CategoryID")
	}

	category, err := getCategoryByID(categoryID)
	if err != nil {
		return nil, err
	}

	temp := &entity.Product{
		EntityID:     entityID,
		Name:         strings.Trim(p.GetName(), " "),
		Description:  strings.Trim(p.GetDescription(), " "),
		Brand:        strings.Trim(p.GetBrand(), " "),
		UnitPrice:    p.GetUnitPrice(),
		MAP:          p.GetMAP(),
		ImageURL:     p.GetImageURL(),
		SKUNumber:    p.GetSKUNumber(),
		UPC:          p.GetUPC(),
		CategoryInfo: category,
		IsActive:     p.GetIsActive(),
	}

	for _, ing := range p.GetIngredient() {
		m, err := ing.Marshal()
		if logger.CheckError(err) {
			continue
		}
		temp.Ingredient = append(temp.Ingredient, string(m))
	}

	for _, nut := range p.GetNutrients() {
		m, err := nut.Marshal()
		if logger.CheckError(err) {
			continue
		}
		temp.Nutrients = append(temp.Nutrients, string(m))
	}
	if db := common.DB.Save(temp).Scan(&temp); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to save product")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ProductChange, temp.ID.String()))
	return temp, nil
}

//GetAllProducts get all products by entityID
func GetAllProducts(entityID uuid.UUID) ([]*entity.Product, error) {
	return getProductsByEntity(entityID)
}

//GetProductsByIDs get products by entityID and ids
func GetProductsByIDs(ids []uuid.UUID) ([]*entity.Product, error) {
	return getProductsByIDs(ids)
}

//UpdateProduct Update product
func UpdateProduct(productID uuid.UUID, p *module.CreateProductItem) (*entity.Product, error) {
	prod, err := getProductByID(productID)
	if err != nil {
		return nil, err
	}
	categoryID, err := uuid.FromString(p.GetCategoryID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad CategoryID")
	}

	category, err := getCategoryByID(categoryID)
	if err != nil {
		return nil, err
	}

	prod.Name = strings.Trim(p.GetName(), " ")
	prod.Description = strings.Trim(p.GetDescription(), " ")
	prod.Brand = strings.Trim(p.GetBrand(), " ")
	prod.UnitPrice = p.GetUnitPrice()
	prod.MAP = p.GetMAP()
	prod.ImageURL = p.GetImageURL()
	prod.SKUNumber = p.GetSKUNumber()
	prod.UPC = p.GetUPC()
	prod.CategoryInfo = category
	prod.IsActive = p.GetIsActive()

	var tempIngredient []string
	var tempNutrients []string
	prod.Ingredient = tempIngredient
	prod.Nutrients = tempNutrients

	for _, ing := range p.GetIngredient() {
		m, err := ing.Marshal()
		if logger.CheckError(err) {
			continue
		}
		prod.Ingredient = append(prod.Ingredient, string(m))
	}

	for _, nut := range p.GetNutrients() {
		m, err := nut.Marshal()
		if logger.CheckError(err) {
			continue
		}
		prod.Nutrients = append(prod.Nutrients, string(m))
	}

	if db := common.DB.Save(prod).Scan(&prod); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to update product")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ProductChange, prod.ID.String()))
	return prod, nil
}

//DeleteProduct by entityID, productID
func DeleteProduct(productID uuid.UUID) error {
	prod, err := getProductByID(productID)
	if err != nil {
		return err
	}

	if db := common.DB.Delete(prod); logger.CheckError(db.Error) {
		return status.Error(codes.Internal, "Failed to delete product")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ProductChange, prod.ID.String()))
	return nil
}

//CreateInventory create inventory
func CreateInventory(entityID, productID uuid.UUID, wareHouse string, amount uint64) (*entity.Inventory, error) {
	_, err := getProductByEntityAndID(entityID, productID)
	if err != nil {
		return nil, err
	}

	inventory := &entity.Inventory{
		ProductID: productID,
		EntityID:  entityID,
		Warehouse: wareHouse,
		Amount:    amount,
	}

	if db := common.DB.Save(inventory).Scan(&inventory); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to create inventory")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ProductChange, productID.String()))

	return inventory, nil
}

//UpdateInventory update inventory by entityID and inventoryID
func UpdateInventory(productID, inventoryID uuid.UUID, wareHouse string, amount uint64) (*entity.Inventory, error) {
	product, err := getProductByID(productID)
	if err != nil {
		return nil, err
	}

	inventory := &entity.Inventory{}

	for _, inv := range product.InventoryInfo {
		if inv.ID == inventoryID {
			inventory = inv
			break
		}
	}

	if inventory.ID == uuid.Nil {
		return nil, status.Error(codes.NotFound, "")
	}

	inventory.Warehouse = wareHouse
	inventory.Amount = amount

	if db := common.DB.Save(inventory).Scan(&inventory); logger.CheckError(db.Error) {
		return nil, status.Error(codes.Internal, "Failed to update inventory")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ProductChange, inventory.ProductID.String()))

	return inventory, nil
}

//UnaryServerAuthInterceptor address unary interceptor
func UnaryServerAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx := ctx
		var err error
		newCtx, err = common.Authenticate(ctx)
		if err != nil {
			return nil, err
		}
		return handler(newCtx, req)
	}
}

//StreamServerAuthInterceptor address stream interceptor
func StreamServerAuthInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		newCtx := stream.Context()
		var err error
		newCtx, err = common.Authenticate(stream.Context())
		if err != nil {
			return err
		}
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		return handler(srv, wrapped)
	}
}

func getProductsByEntity(entityID uuid.UUID) ([]*entity.Product, error) {
	products := []*entity.Product{}
	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.ProductHashMap, redisClient.RedisMapKeys.GetAllProductsByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return products, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get products")
	}
	if len(mapVals) < 1 {
		return products, nil
	}
	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return products, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get products")
	}
	for _, v := range vals {
		if v != nil {
			product := &entity.Product{}
			err = product.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				products = append(products, product)
			}
		}
	}
	return products, nil
}

func getProductByEntityAndID(entityID, ID uuid.UUID) (*entity.Product, error) {
	product := &entity.Product{}
	mapKey := redisClient.RedisMapKeys.GetProductMapKey(entityID.String(), ID.String())
	mapVal, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.ProductHashMap, mapKey)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get product ID: %s, Entity ID: %s", ID.String(), entityID.String()))
	}
	err = redisClient.GetRedisValue(mapVal, product)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get product ID: %s", ID.String()))
	}
	return product, nil
}

func getProductByID(id uuid.UUID) (*entity.Product, error) {
	product := &entity.Product{}
	key := redisClient.RedisMapKeys.GetProductKey(id.String())
	err := redisClient.GetRedisValue(key, product)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get product ID: %s", id.String()))
	}
	return product, nil
}

func getProductsByIDs(ids []uuid.UUID) ([]*entity.Product, error) {
	products := []*entity.Product{}
	var keys []string
	for _, id := range ids {
		keys = append(keys, redisClient.RedisMapKeys.GetProductKey(id.String()))
	}
	vals, err := redisClient.GetRedisValues(keys)
	if err == redis.Nil {
		return products, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get products")
	}
	for _, v := range vals {
		if v != nil {
			product := &entity.Product{}
			err = product.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				products = append(products, product)
			}
		}
	}
	return products, nil
}

func getCategoryByID(id uuid.UUID) (*entity.Category, error) {
	category := &entity.Category{}
	key := redisClient.RedisMapKeys.GetCategoryKey(id.String())
	err := redisClient.GetRedisValue(key, category)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get category ID: %v", id))
	}
	return category, nil
}

func getCategoriesByEntityID(entityID uuid.UUID) ([]*entity.Category, error) {
	categories := []*entity.Category{}
	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.CategoryHashMap,
		redisClient.RedisMapKeys.GetCategoriesByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return categories, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get categories")
	}
	if len(mapVals) < 1 {
		return categories, nil
	}
	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return categories, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get categories")
	}
	for _, v := range vals {
		if v != nil {
			category := &entity.Category{}
			err = category.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				categories = append(categories, category)
			}
		}
	}
	return categories, nil
}
