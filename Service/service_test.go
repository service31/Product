package service

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	entity "gitlab.com/service31/Data/Entity/Product"
)

func TestMain(m *testing.M) {
	SetupTesting()
	common.DB.AutoMigrate(&entity.Product{}, &entity.Category{}, &entity.Inventory{})
	defer DoneTesting()
	m.Run()
}

func TestCreateCategory(t *testing.T) {
	in, entityID := GetTestCreateCategoryData()

	c, err := CreateCategory(entityID, in)

	if err != nil {
		t.Error(err)
	} else if c.Name != in.GetName() {
		t.Errorf("Category name: %s, but got %s", in.GetName(), c.Name)
	}
}

func TestUpdateCategory(t *testing.T) {
	in, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, in)
	if err != nil {
		t.Error(err)
	} else if c.Name != in.GetName() {
		t.Errorf("Category name: %s, but got %s", in.GetName(), c.Name)
	}
	SetupCategory(c)

	updateIn := GetTestUpdateCategoryData(c.ID)
	updatedCategory, err := UpdateCategory(updateIn)
	if err != nil {
		t.Error(err)
	} else if updatedCategory.Name == c.Name {
		t.Errorf("Name before update: %s is same as after", c.Name)
	}
}

func TestCreateProduct(t *testing.T) {
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)

	p, err := CreateProduct(entityID, requests[0])

	if err != nil {
		t.Error(err)
	} else if p.Name != requests[0].GetProduct().GetName() {
		t.Errorf("Product name: %s, but got %s", requests[0].GetProduct().GetName(), p.Name)
	}
}

func TestGetAllProducts(t *testing.T) {
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)
	for _, r := range requests {
		p, err := CreateProduct(entityID, r)
		if err != nil {
			t.Error(err)
		} else if p.Name != r.GetProduct().GetName() {
			t.Errorf("Product name: %s, but got %s", r.GetProduct().GetName(), p.Name)
		}
		SetupProduct(p)
	}

	ps, err := GetAllProducts(entityID)

	if err != nil {
		t.Error(err)
	} else if len(ps) != len(requests) {
		t.Errorf("Created %d but got %d", len(requests), len(ps))
	}
}

func TestGetProductsByIDs(t *testing.T) {
	var ids []uuid.UUID
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)
	for _, r := range requests {
		p, err := CreateProduct(entityID, r)
		if err != nil {
			t.Error(err)
		} else if p.Name != r.GetProduct().GetName() {
			t.Errorf("Product name: %s, but got %s", r.GetProduct().GetName(), p.Name)
		}
		SetupProduct(p)
		ids = append(ids, p.ID)
	}

	ps, err := GetProductsByIDs(ids)

	if err != nil {
		t.Error(err)
	} else if len(ps) != len(requests) {
		t.Errorf("Created %d but got %d", len(requests), len(ps))
	}
}

func TestUpdateProduct(t *testing.T) {
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)
	p, err := CreateProduct(entityID, requests[0])
	if err != nil {
		t.Error(err)
	} else if p.Name != requests[0].GetProduct().GetName() {
		t.Errorf("Product name: %s, but got %s", requests[0].GetProduct().GetName(), p.Name)
	}
	SetupProduct(p)

	product, err := UpdateProduct(p.ID, requests[1].GetProduct())
	if err != nil {
		t.Error(err)
	} else if product.Name != requests[1].GetProduct().GetName() {
		t.Errorf("Product name is not updated")
	}
}

func TestUpdateProductWithNoProduct(t *testing.T) {
	_, entityID := GetTestCreateCategoryData()
	requests, entityID := GetNewProductData(entityID)

	_, err := UpdateProduct(entityID, requests[0].GetProduct())
	if err == nil {
		t.Error("No error returned")
	}
}

func TestDeleteProduct(t *testing.T) {
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)
	p, err := CreateProduct(entityID, requests[0])
	if err != nil {
		t.Error(err)
	} else if p.Name != requests[0].GetProduct().GetName() {
		t.Errorf("Product name: %s, but got %s", requests[0].GetProduct().GetName(), p.Name)
	}
	SetupProduct(p)

	err = DeleteProduct(p.ID)
	if err != nil {
		t.Error(err)
	}
}

func TestDeleteProductWithoutProduct(t *testing.T) {
	_, entityID := GetTestCreateCategoryData()
	err := DeleteProduct(entityID)
	if err == nil {
		t.Error(err)
	}
}

func TestCreateInventory(t *testing.T) {
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)
	p, err := CreateProduct(entityID, requests[0])
	if err != nil {
		t.Error(err)
	} else if p.Name != requests[0].GetProduct().GetName() {
		t.Errorf("Product name: %s, but got %s", requests[0].GetProduct().GetName(), p.Name)
	}
	SetupProduct(p)
	warehouse, amount := GetNewInventoryData()

	i, err := CreateInventory(p.EntityID, p.ID, warehouse, amount)

	if err != nil {
		t.Error(err)
	} else if i.Amount < 1 {
		t.Error("Inventory < 1")
	}
}

func TestCreateInventoryWithInvalidProduct(t *testing.T) {
	_, entityID := GetTestCreateCategoryData()
	warehouse, amount := GetNewInventoryData()

	_, err := CreateInventory(entityID, entityID, warehouse, amount)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdateInventory(t *testing.T) {
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)
	p, err := CreateProduct(entityID, requests[0])
	if err != nil {
		t.Error(err)
	} else if p.Name != requests[0].GetProduct().GetName() {
		t.Errorf("Product name: %s, but got %s", requests[0].GetProduct().GetName(), p.Name)
	}
	SetupProduct(p)
	warehouse, amount := GetNewInventoryData()
	i, err := CreateInventory(p.EntityID, p.ID, warehouse, amount)
	if err != nil {
		t.Error(err)
	} else if i.Amount < 1 {
		t.Error("Inventory < 1")
	}
	p.InventoryInfo = append(p.InventoryInfo, i)
	SetupProduct(p)
	amount = 1

	inv, err := UpdateInventory(p.ID, i.ID, warehouse, amount)
	if err != nil {
		t.Error(err)
	} else if inv.Amount != amount {
		t.Error("Amount is not updated")
	}
}

func TestUpdateInventoryNotFound(t *testing.T) {
	categoryIn, entityID := GetTestCreateCategoryData()
	c, err := CreateCategory(entityID, categoryIn)
	if err != nil {
		t.Error(err)
	} else if c.Name != categoryIn.GetName() {
		t.Errorf("Category name: %s, but got %s", categoryIn.GetName(), c.Name)
	}
	SetupCategory(c)
	requests, entityID := GetNewProductData(c.ID)
	p, err := CreateProduct(entityID, requests[0])
	if err != nil {
		t.Error(err)
	} else if p.Name != requests[0].GetProduct().GetName() {
		t.Errorf("Product name: %s, but got %s", requests[0].GetProduct().GetName(), p.Name)
	}
	SetupProduct(p)
	warehouse, amount := GetNewInventoryData()

	_, err = UpdateInventory(p.ID, p.ID, warehouse, amount)
	if err == nil {
		t.Error("No error returned")
	}
}
