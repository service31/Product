package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"

	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	entity "gitlab.com/service31/Data/Entity/Product"
	module "gitlab.com/service31/Data/Module/Product"
	service "gitlab.com/service31/Product/Service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var (
	echoEndpoint  = flag.String("endpoint", "localhost:8000", "endpoint")
	serverChannel chan int
)

type server struct {
	module.UnimplementedProductServiceServer
}

func (s *server) CreateCategory(ctx context.Context, in *module.CreateCategoryRequest) (*module.CategoryDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	category, err := service.CreateCategory(entityID, in)
	if err != nil {
		return nil, err
	}
	return category.ToDTO(), nil
}

func (s *server) UpdateCategory(ctx context.Context, in *module.UpdateCategoryRequest) (*module.CategoryDTO, error) {
	category, err := service.UpdateCategory(in)
	if err != nil {
		return nil, err
	}
	return category.ToDTO(), nil
}

func (s *server) CreateProduct(ctx context.Context, in *module.CreateProductRequest) (*module.DetailedProductDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	product, err := service.CreateProduct(entityID, in)
	if err != nil {
		return nil, err
	}
	dto, err := product.ToDTO()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) GetAllProducts(ctx context.Context, in *module.EntityIDRequest) (*module.RepeatedProductDetailedDTOResponse, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	products, err := service.GetAllProducts(entityID)
	if err != nil {
		return nil, err
	}
	var dtos *module.RepeatedProductDetailedDTOResponse
	for _, p := range products {
		dto, err := p.ToDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, fmt.Sprintf("ProductID: %s has error. ErrorMessage: %s", p.ID.String(), err.Error()))
		}
		dtos.Results = append(dtos.Results, dto)
	}
	return dtos, nil
}

func (s *server) GetProductsByIDs(ctx context.Context, in *module.GetProductsByIDsRequest) (*module.RepeatedProductDetailedDTOResponse, error) {
	var pids []uuid.UUID
	for _, id := range in.GetID() {
		i, err := uuid.FromString(id)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Bad ID: %s", id))
		}
		pids = append(pids, i)
	}
	products, err := service.GetProductsByIDs(pids)
	if err != nil {
		return nil, err
	}
	var dtos *module.RepeatedProductDetailedDTOResponse
	for _, p := range products {
		dto, err := p.ToDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, fmt.Sprintf("ProductID: %s has error. ErrorMessage: %s", p.ID.String(), err.Error()))
		}
		dtos.Results = append(dtos.Results, dto)
	}
	return dtos, nil
}

func (s *server) UpdateProduct(ctx context.Context, in *module.UpdateProductRequest) (*module.DetailedProductDTO, error) {
	productID, err := uuid.FromString(in.GetID())
	if err != nil || productID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ProductID")
	}

	product, err := service.UpdateProduct(productID, in.GetProduct())
	if err != nil {
		return nil, err
	}
	dto, err := product.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}
func (s *server) DeleteProduct(ctx context.Context, in *module.DeleteProductRequest) (*module.BoolResponse, error) {
	productID, err := uuid.FromString(in.GetID())
	if err != nil || productID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ProductID")
	}

	err = service.DeleteProduct(productID)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: true}, nil
}

func (s *server) CreateInventory(ctx context.Context, in *module.CreateInventoryRequest) (*module.InventoryDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	productID, err := uuid.FromString(in.GetProductID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ProductID")
	}

	inv, err := service.CreateInventory(entityID, productID, in.GetWarehouse(), in.GetAmount())
	if err != nil {
		return nil, err
	}
	return inv.ToDTO(), nil
}

func (s *server) UpdateInventory(ctx context.Context, in *module.UpdateInventoryRequest) (*module.InventoryDTO, error) {
	productID, err := uuid.FromString(in.GetProductID())
	if err != nil || productID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ProductID")
	}
	inventoryID, err := uuid.FromString(in.GetInventoryID())
	if err != nil || inventoryID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad InventoryID")
	}
	inv, err := service.UpdateInventory(productID, inventoryID, in.GetWarehouse(), in.GetAmount())
	if err != nil {
		return nil, err
	}
	return inv.ToDTO(), nil
}

func runGRPCServer() {
	lis, err := net.Listen("tcp", ":8000")
	logger.CheckFatal(err)

	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_validator.StreamServerInterceptor(),
				service.StreamServerAuthInterceptor(),
				grpc_zap.StreamServerInterceptor(logger.GetLogger()),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_validator.UnaryServerInterceptor(),
				service.UnaryServerAuthInterceptor(),
				grpc_zap.UnaryServerInterceptor(logger.GetLogger()),
			)),
	)
	module.RegisterProductServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		logger.Error(fmt.Sprintf("failed to serve: %v", err))
	}

	serverChannel <- 1
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./Swagger/product.swagger.json")
}

func runRestServer() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	gwmux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := module.RegisterProductServiceHandlerFromEndpoint(ctx, gwmux, *echoEndpoint, opts)
	if logger.CheckError(err) {
		serverChannel <- 999
		return
	}
	mux.Handle("/", gwmux)
	mux.HandleFunc("/swagger.json", serveSwagger)
	fs := http.FileServer(http.Dir("Swagger/swagger-ui"))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui", fs))

	logger.CheckError(http.ListenAndServe(":80", mux))
	serverChannel <- 2
}

func main() {
	common.Bootstrap()
	common.DB.AutoMigrate(&entity.Product{}, &entity.Category{}, &entity.Inventory{})
	serverChannel = make(chan int)
	go runGRPCServer()
	go runRestServer()
	for {
		serverType := <-serverChannel
		if serverType == 1 {
			//GRPC
			logger.Info("Restarting GRPC endpoint")
			go runGRPCServer()
		} else if serverType == 2 {
			//Rest
			logger.Info("Restarting Rest endpoint")
			go runRestServer()
		} else {
			logger.Fatal("Failed to start some endPoint")
		}
	}
}
