module gitlab.com/service31/Product

go 1.13

require (
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/gogo/status v1.1.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.14.4
	github.com/satori/go.uuid v1.2.0
	gitlab.com/service31/Common v0.0.0-20200420013348-5f3b6808cd88
	gitlab.com/service31/Data v0.0.0-20200420013210-e43da3937136
	google.golang.org/grpc v1.28.1
)
